﻿
; ███████╗ ██████╗ ██████╗ ██╗   ██╗███╗   ███╗███████╗███████╗███████╗██████╗ 
; ██╔════╝██╔═══██╗██╔══██╗██║   ██║████╗ ████║██╔════╝██╔════╝██╔════╝██╔══██╗
; █████╗  ██║   ██║██████╔╝██║   ██║██╔████╔██║█████╗  █████╗  █████╗  ██║  ██║
; ██╔══╝  ██║   ██║██╔══██╗██║   ██║██║╚██╔╝██║██╔══╝  ██╔══╝  ██╔══╝  ██║  ██║
; ██║     ╚██████╔╝██║  ██║╚██████╔╝██║ ╚═╝ ██║██║     ███████╗███████╗██████╔╝
; ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚═╝     ╚══════╝╚══════╝╚═════╝ 
;                                                                              
; 
; ██████╗ ██╗   ██╗    ██████╗ ███████╗███████╗ ██████╗ ██████╗ 
; ██╔══██╗╚██╗ ██╔╝    ██╔══██╗██╔════╝╚══███╔╝██╔═══██╗██╔══██╗
; ██████╔╝ ╚████╔╝     ██║  ██║█████╗    ███╔╝ ██║   ██║██████╔╝
; ██╔══██╗  ╚██╔╝      ██║  ██║██╔══╝   ███╔╝  ██║   ██║██╔══██╗
; ██████╔╝   ██║       ██████╔╝███████╗███████╗╚██████╔╝██║  ██║
; ╚═════╝    ╚═╝       ╚═════╝ ╚══════╝╚══════╝ ╚═════╝ ╚═╝  ╚═╝
                                                              

;### Abfrage ob als Admin gestartet wird oder nicht! ###
if(!A_IsAdmin){
   Run *RunAs "%A_ScriptFullPath%"  ; Requires v1.0.92.01+
   ExitApp
}

#SingleInstance, Force
#UseHook, On
#NoEnv
#Persistent

#MaxThreads 50
#KeyHistory 0
SetWorkingDir %A_ScriptDir%

ListLines Off
Process, Priority, , H ;if unstable, comment or remove this line
SetBatchLines, -1
SetKeyDelay, -1, -1
SetMouseDelay, -1
SetDefaultMouseSpeed, 0
SetWinDelay, -1
SetControlDelay, -1
SendMode Input
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

#include %A_ScriptDir%\SAMP-API-R17.ahk

global feed_url := "https://life-of-german.org/forum/board-feed/"
global feed := DownloadToString(feed_url)
global feed_title_old
global feed_creator_old


Settimer, RSSfeed, 60000

AddChatmessage("{f26522}ForumFeed erfolgreich gestartet!")

RSSFeed:
	
    count := 0
    Loop, parse, feed, `n, `r 
    {
        if(InStr(A_LoopField, "<title><![CDATA[") && !InStr(A_LoopField, "Life of German"))
        {
            RegExMatch(A_LoopField, "<title><!\[CDATA\[(.*)\]\]></title>", feed_title)
        } 
        if(InStr(A_LoopField, "<dc:creator><![CDATA"))
        {
            RegExMatch(A_LoopField, "<dc:creator><!\[CDATA\[(.*)\]\]></dc:creator>", feed_creator)
            count++
			
			; Falls der Beitrag nicht dem von vorher entspricht - also er neu ist - sende neuen Beitrag.
			if not (InStr(feed_creator_old, feed_creator1) AND InStr(feed_title_old, feed_title1))
				AddChatmessage("{f26522}Forum: {ffffff}Neuer Beitrag von {f26522}" feed_creator1 " {ffffff}im Thread {f26522}" feed_title1)
            
			; Speichere neusten Beitrag in Variable um oben auf neuen Beitrag zu testen.
			feed_title_old := feed_title
			feed_creator_old := feed_creator
        }
        if(count == 1)
            break
	}
return